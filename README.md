# Yuliia Terentieva's README

Hi, I'm Yuliia. Welcome to my README!
I'm the Quality Assurence Engineer, Scrum master.

# Who I am
- I was born in Latvia but all my conscious life I lived in Ukraine, [Dnipro](https://en.wikipedia.org/wiki/Dnipro). 
- Currently I live in Krakow, Poland with my two kids.
- I received a degree in Cyber economy from the Mountain Academy (now Polytechnic) of Dnipro. 
- I started my career at a regional gas company, the most of the way I worked in the management.
- My IT career was started in 2020 from SosftServe QC trainee on the helthcare projec.
- I already had chance to try myself in UI testing, Back-end testing (Java), in healthcare project, BI project, as a tester VS Scrum master
- Now I'm on my way of development my skills in Java automation testing


# Working with me
- I am a very sincere person, sociable, open to new acquaintances and not afraid of challenges
- I'm optimistic, which is my superpower most of the time
- I'm willing to help and greatly accept suggestions from others
- I think that I rather a stress-resistant and critical-receptive person

<p>
<details>
<summary>Click for more info!</summary>


# My passions
- I adore my kids and all my free time I aim to spend with them
You will never catch up with us at home on the weekends, doesn't matter what the weather will be, we obviously will be out:
at the forest, park, cinema, cafe, zoo, theatre, museum, playground... we have a lot of ideas to have fun!

</details>
</p>
